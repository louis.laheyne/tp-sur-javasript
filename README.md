# TP sur javaSript

Mes TP en JavaScript

> * Auteur : Louis Laheyne
> * Date : 06/04/2021

## Sommaire

  - 1.Introduction
    - [1.1 HelloWorld](1-introduction/1.1-HelloWorld-js/index.html)

____

  - 2.variable et operateur  
    - [2.1 calcul surface](2-variables-et-operateur/2.1-calcul_surface/index.html)
    - [2.2 calcul imc](2-variables-et-operateur/2.2-calcul_imc/index.html)
    - [2.3 conversion celcius farenheit](2-variables-et-operateur/2.3-conv_cel_F/index.html)

____

  - 3.Structure de controle  
      - [3.1 interpretation imc](3-structures_de_controle/3.1-interpretation_IMC/index.html)
      - [3.2 suite de syracuse](3-structures_de_controle/3.2-suite_syracuse/index.html)
      - [3.3 calcul factorielle](3-structures_de_controle/3.3-calcul_factorielle/index.html)
      - [3.4 conversion euro dollars](3-structures_de_controle/3.4-conversion_euro_dollars/index.html)
      - [3.5 nombre triples](3-structures_de_controle/3.5-nombres_triples/index.html)
      - [3.6 suite de fibonacci](3-structures_de_controle/3.6-suite_de_fibonacci/index.html)
      - [3.7 table de multiplication](3-structures_de_controle/3.7-table_multiplication/index.html)

____

  - 4.les fonction  
      - [4.1 calcul et interpretation d'IMC](4-fonctions/4.1-calcul_et_interpretation_IMC/index.html)
      - [4.1.1 calcul et interpretation IMC imbrique](4-fonctons/4.1.1-calcul_et_interpretation_IMC_imbrique/index.html)
      - [4.2 calcule d'un temps de parcours](4-fonctions/4.2-calcul_tps_parcours_trajet/index.html)
      - [4.3 recherche de nombre multiple de 3](4.3-recherche_nb_multiple_3/../4-fonctions/4.3-recherche_nb_multiple_3/index.html)

____

  - 5.objets
      - [5.1 creation obj IMC](5-objets/5.1-creation_obj_IMC/index.html)
      - [5.2 creation obj IMC V2](5-objets/5.2-creation_obj_IMC_V2/index.html)
      - [5.3 constructeur obj IMC](5-objets/5.3-constructeur_obj_IMC/index.html)
