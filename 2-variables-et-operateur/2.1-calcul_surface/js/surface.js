//fichier surface.js
//calcul et affiche d'une surface
//Louis Laheyne 14/04/2021 

let longueur = 2 ; //pour la longueur
let largeur = 3 ; // pour la largeur
let surface = 0 ; // pour la surface

surface = longueur * largeur; // calcul de la surface

//affichage
console.log("longueur de la piece : " +longueur +"m")
console.log("largeur de la piece : " +largeur +"m")
console.log("surface de la piece : " +surface +"m²")