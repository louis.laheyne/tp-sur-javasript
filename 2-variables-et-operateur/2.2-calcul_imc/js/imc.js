//fichier imc.js
//affichage de l'IMC
//Louis Laheyne 14/04/2021 

let taille = 160; // taille en cm
let poids =100;    // poids en kg
let imc = 0 ; // pour le resultat du calcul de l'imc 

imc = poids / ((taille / 100)*(taille / 100)); //calcul de l'IMC

//affichage  
console.log("calcul de l'IMC : ")
console.log("Taille : " +taille + "cm")
console.log("Poids : " +poids + "kg")
console.log("IMC : " +imc.toFixed(1))