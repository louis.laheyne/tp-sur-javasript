//fichier conversion.js
//affichage de la conversion celcius/ferenheit pour une temperature donnee
//Louis Laheyne 14/04/2021 

let temperatureCel = 22.6 ; // temperature en celcius
let temperatureFar = 0 ; // temperature en farenheit

temperatureFar = temperatureCel*1.8+32 ; // calcul de convertion
//affichage
console.log("la temperature de" +temperatureCel+"°C correspond à une temperature de " +temperatureFar.toFixed(1) +"°F")