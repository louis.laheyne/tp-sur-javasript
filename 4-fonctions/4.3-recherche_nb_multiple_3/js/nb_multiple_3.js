// fichier nb_multiple_3.js
// affiche les multiple de 3 de 0 a un chiffre donner
// Laheyne Louis 25/04/2021
let nbVal = 20 ;
let nbMultiple = 0 ;
let affichage = "" ;

function rechercher_Mult3(prmNbVal){
    let affichage = 0 ;
    for(let i = 0; i < prmNbVal;i++){
        if(i % 3 == 0){
            affichage = affichage + "-" + i ;
        }
    }
    return affichage ;
}

affichage = rechercher_Mult3(nbVal);

console.log("recherche des multiple de 3 : ")
console.log("valeur limite de la recherche : 20 ")
console.log(affichage);