// fichier tps_parcours.js
// calculer et afficher un temps de parcours
// Laheyne Louis 25/04/2021

function calculerTempsParcoursSec(prmVitesse, prmDistance) {
    let tpsSeconde = 0;
    tpsSeconde = (prmDistance / prmVitesse) * 3600;
    return tpsSeconde;
}

function convertir_h_min_sec(prmTpsSec) {
    let heure = 0;
    let min = 0;
    let sec = 0;
    let affchage = "";

    heure = prmTpsSec / 3600;
    min = (prmTpsSec % 3600) / 60;
    sec = (prmTpsSec % 3600) % 60;

    affichage = Math.floor(heure) + " h " + Math.floor(min) + " min " + sec + " s";
    return affichage;
}

let vitesse = 90;
let distance = 500;
let tpsSeconde = 0;
let tpsHeureMinSec = 0;

tpsSeconde = calculerTempsParcoursSec(vitesse, distance)
tpsHeureMinSec = convertir_h_min_sec(tpsSeconde);
console.log("Calcul du temps de parcours d'un trajet : ");
console.log("   Vitesse moyenne (en km/h) : " + vitesse);
console.log("   Distance à parcourir (en km) : " + distance);
console.log("A " + vitesse + "km/h, une distance de " + distance + " km est parcourue en " + tpsHeureMinSec);
