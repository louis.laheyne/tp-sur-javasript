//fichier fonction_IMC.js
//utilisation de fonction pour le calcule et l'interpretation de l'imc
//Laheyne Louis 23/04/2021

function decrire_corpulence(prmTaille, prmPoids) {
    let interpretation = "";
    let affichage = "";

    function calculerIMC(prmTaille, prmPoids) {
        let valIMC;
        valIMC = prmPoids / ((prmTaille / 100) * (prmTaille / 100)); //calcul de l'IMC
        return valIMC;
    }
    let IMC = 0;
    IMC = calculerIMC(prmTaille, prmPoids);

    function interpreterIMC(prmIMC) {
        let interpretation = "";
        if (prmIMC < 16.5) { //si l'imc est inferieur a 16.5 la personne est en denutrition
            interpretation = "Interpetation de l'IMC : Denutrition";
        }
        if ((prmIMC >= 16.5) && (prmIMC < 18.5)) { //si l'imc est superieur a 16.5 et inferieur a 18.5 la personne est en maigreur
            interpretation = "Interpetation de l'IMC : Maigreur";
        }
        if ((prmIMC >= 18.5) && (prmIMC < 25)) { //si l'imc est superieur a 18.5 et inferieur a 25 la personne a une corpulence normale
            interpretation = "Interpetation de l'IMC : corpulence normale";
        }
        if ((prmIMC >= 25) && (prmIMC < 30)) { //si l'imc est superieur a 25 et inferieur a 30 la personne est en surpoids
            interpretation = "Interpetation de l'IMC : surpoids";
        }
        if ((prmIMC >= 30) && (prmIMC < 35)) { //si l'imc est superieur a 30 et inferieur a 35 la personne est en obesite modere
            interpretation = "Interpetation de l'IMC : Obesite modere";
        }
        if ((prmIMC >= 35) && (prmIMC <= 40)) { //si l'imc est superieur a 35 et inferieur a 4 la personne est en obesite severe
            interpretation = "Interpetation de l'IMC : Obesite severe";
        }
        if (prmIMC > 40) { //si l'imc est superieur a 40  la personne est en obesite morbide
            interpretation = "Interpetation de l'IMC : obesite morbide";
        }
        return interpretation;

    }
    interpretation = interpreterIMC(IMC) ;
    affichage = "votre IMC est égal à " + IMC.toFixed(2) + " : vous êtes en état de " + interpretation ;
    return affichage ;
}
let taille = 175;
let poids = 100;
let affichage = "";
affichage = decrire_corpulence(taille, poids);

console.log("Calcul de l'IMC :")
console.log("Taille " + taille + "cm");
console.log("poids " + poids + "kg");
console.log(affichage);