//fichier syracuse.js
//affichage de la suite de syracuse a partir d'un nombre donnee
//Louis Laheyne 14/04/2021 

let N = 14 ; //valeur de depart
let valeur = " " + N ; // chaine de caractere pour l'affichage
let nbTour = 0 ; //compte le nombre de tour de boucle effectuer
let valeurMax = N ; // valeur max trouver dans la chaine 

console.log("suite de syracuse pour" + N + ": " ); // affichage
//affichage d'un message d'erreur si N est inferieur a 1
if (N < 1){
    console.log("erreur valeur de N inferieur a 1 " );
}
//sinon tant que N est superieur a 1
else{
    while( N > 1 ){
        // valeurMax prend la valeur de N si N est superieur a valeurMax 
        if(N > valeurMax){
            valeurMax = N ;
        }
        //si N est pair divise N par 2
        if( N % 2 == 0 ){
            N = N / 2 ;
        }
        //sinon multiplier N par 3 et ajouter 1
        else{
            N = (N * 3) + 1 ;
        }
        valeur = valeur + "-"+ N ; // ajouter a la suite de la chaine de caractere la valeur N
        nbTour ++ ; // ajout de 1 tour a nbTour
    }
}
// affichage
console.log(valeur) ;
console.log("temps de vol = " + nbTour) ;
console.log("Altitude maximale = " + valeurMax) ;