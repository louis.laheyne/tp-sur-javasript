// fichier suite_de_fibonacci.js
// affiche la suite de fibonacci sur les 17 premier nombre
// Louis Laheyne 14/04/2021

let nb1 = 0 ; //variable pour la premieres valeur
let nb2 = 1 ; //variable pour la deuxiemes valeur
let res = 0 ; //variable pour le resultat du calcul
let affichage = "" + nb1 + " " + nb2 ; // pour l'affichage

//pour les 17 premiere chiffre
for(let i = 0 ; i < 17 ; i++){
    res = nb1 + nb2 ; //calcul
    nb1 = nb2 ; //on remplace la premieres valeur par la 2e pour le prochain calcul
    nb2 = res ; //on remplace la 2e valeur par le resultat du calcul pour le prochain calcul 
    affichage = affichage + " " + res ;// pour l'affichage
}
//affichage
console.log("suite de fibonacci : " + affichage) ;