//fichier factorielle.js
//affichage du calcul d'une factorielle donné
//Louis Laheyne 14/04/2021 

let N = 10 ; // la factorielle
let result = 1 ; // resultat du calcul 
let varAffichage = " " + result ; // pour l'affichage 

// pour i allant de 1 a N on multiplie le resultat par la valeur de i  
for (let i = 1; i <= N ; i++) { 
    result = i * result ;
    varAffichage = varAffichage + " X " + i ; // pour l'affichage 
}
// affichage 
console.log(N + "!=" + varAffichage + " = " + result)