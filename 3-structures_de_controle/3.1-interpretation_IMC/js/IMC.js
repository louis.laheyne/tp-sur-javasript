//fichier IMC.js
//affichage de l'IMC et de sont interpretations
//Louis Laheyne 14/04/2021 

let taille = 180; // taille en cm
let poids = 90;    // poids en kg
let imc = 0 ; // pour le resultat du calcul de l'imc 

imc = poids / ((taille / 100)*(taille / 100)); //calcul de l'IMC

//affichage  
console.log("calcul de l'IMC : ")
console.log("Taille : " +taille + "cm")
console.log("Poids : " +poids + "kg")
console.log("IMC : " +imc.toFixed(1))

if(imc<16.5){ //si l'imc est inferieur a 16.5 la personne est en denutrition
    console.log("Interpetation de l'IMC : Denutrition")
}
if((imc >= 16.5) && (imc < 18.5)){ //si l'imc est superieur a 16.5 et inferieur a 18.5 la personne est en maigreur
    console.log("Interpetation de l'IMC : Maigreur")
}
if((imc >=18.5) && (imc < 25)){ //si l'imc est superieur a 18.5 et inferieur a 25 la personne a une corpulence normale
    console.log("Interpetation de l'IMC : corpulence normale")
}
if((imc >=25) && (imc < 30)){ //si l'imc est superieur a 25 et inferieur a 30 la personne est en surpoids
    console.log("Interpetation de l'IMC : surpoids")
}
if((imc >=30) && (imc < 35)){ //si l'imc est superieur a 30 et inferieur a 35 la personne est en obesite modere
    console.log("Interpetation de l'IMC : Obesite modere")
}
if((imc >=35) && (imc <= 40)){ //si l'imc est superieur a 35 et inferieur a 4 la personne est en obesite severe
    console.log("Interpetation de l'IMC : Obesite severe")
}
if(imc > 40){ //si l'imc est superieur a 40  la personne est en obesite morbide
    console.log("Interpetation de l'IMC : obesite morbide")
}

