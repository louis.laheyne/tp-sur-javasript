// fichier table_multiplication.js
// affiche les 20 premeieres valeur de la table de multiplication de 7 
// Louis Laheyne 14/04/2021

let valOrigin = 7 ;
let res = 0 ;
let affichage = "" ;

for (let i = 1 ; i <= 20 ; i ++) {
    res = valOrigin * i ;

    if (res % 3 == 0 ){
        affichage = affichage  + res + " * " ;
    }else{
        affichage = affichage  + res + " " ;
    }
}
console.log(affichage) ;