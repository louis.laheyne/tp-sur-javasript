//fichier conv_euro_dollars.js
//affichage de la conversion euro/dollars jusqu'a 16384 euros
//Louis Laheyne 14/04/2021 

let values = 1 ; // valeurs en euro
let result = 1.65 ; // resultat de la conversion en dollars

//tant que la valeur en euro et inferieur a 16384 
while ( values <= 16384){ 
    result = values * 1.65 ; // conversion euro/dollars
    // affichage
    console.log(values + " euro(s) = " + result.toFixed(2) + " dollar(s)") ; 
    values = values * 2 ; // passer a la valeur suivante en multipliant par 2
}