//fichier nbTriple.js
//affichage d'une suite de nombre qui commence a un nombre donné et qui triple a chaque tour
//Louis Laheyne 14/04/2021 

let nbDepart = 2 ; //nombre de depart
let res = 0 ; // resultat calcul triple 
let affichage = " " + nbDepart ;
res = nbDepart ;
for(let i = 0; i < 12 ; i++){
    res = res * 3 ;
    affichage = affichage + " " + res
}
console.log("valeur de depart :" + nbDepart) ;
console.log(affichage) ;