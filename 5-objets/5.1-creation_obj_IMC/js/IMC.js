//fichier IMC.js
//utilisation de fonction pour le calcule et l'interpretation de l'imc
//Laheyne Louis 23/04/2021

//creation objet
let objPatient = {
    //propriete
    nom: 'dupond',
    prenom: 'jean',
    age: 30,
    sexe: 'masculin',
    taille: 180,
    poids: 85,

    // methode
    decrire: function () {
        let description; //description du patient
        let tailleMetre = 0; // pour mettre la taille en metre
        tailleMetre = this.taille / 100; // passage de la taille en metre 
        description = "le patient " + this['prenom'] + " " + this['nom'] + " de sexe " + this['sexe'] + " est agé de " + this['age'] + "ans. Il mesure " + tailleMetre + "m et pèse " + this['poids'] + " kg";
        return description;
    },

    calculer_IMC: function () {
        let valIMC; //valeur de l'IMC
        valIMC = this.poids / ((this.taille / 100) * (this.taille / 100)); //calcul de l'IMC
        return valIMC;
    },

    interpreter_IMC: function (prmIMC) {
        let interpretation = ""; // pour l'interpretation

        if (prmIMC < 16.5) { //si l'imc est inferieur a 16.5 la personne est en denutrition
            interpretation = "Interpetation de l'IMC : Denutrition";
        }
        if ((prmIMC >= 16.5) && (prmIMC < 18.5)) { //si l'imc est superieur a 16.5 et inferieur a 18.5 la personne est en maigreur
            interpretation = "Interpetation de l'IMC : Maigreur";
        }
        if ((prmIMC >= 18.5) && (prmIMC < 25)) { //si l'imc est superieur a 18.5 et inferieur a 25 la personne a une corpulence normale
            interpretation = "Interpetation de l'IMC : corpulence normale";
        }
        if ((prmIMC >= 25) && (prmIMC < 30)) { //si l'imc est superieur a 25 et inferieur a 30 la personne est en surpoids
            interpretation = "Interpetation de l'IMC : surpoids";
        }
        if ((prmIMC >= 30) && (prmIMC < 35)) { //si l'imc est superieur a 30 et inferieur a 35 la personne est en obesite modere
            interpretation = "Interpetation de l'IMC : Obesite modere";
        }
        if ((prmIMC >= 35) && (prmIMC <= 40)) { //si l'imc est superieur a 35 et inferieur a 4 la personne est en obesite severe
            interpretation = "Interpetation de l'IMC : Obesite severe";
        }
        if (prmIMC > 40) { //si l'imc est superieur a 40  la personne est en obesite morbide
            interpretation = "Interpetation de l'IMC : obesite morbide";
        }
        return interpretation;
    }
};

let resIMC = 0; // pour la valeur de l'imc
resIMC = objPatient.calculer_IMC();// appel de la fonction dans l'objet pour la valeur de l'imc

//affichage
console.log(objPatient.decrire());
console.log("Son IMC est de :" + objPatient.calculer_IMC().toFixed(2));
console.log("il est en situation de " + objPatient.interpreter_IMC(resIMC));


