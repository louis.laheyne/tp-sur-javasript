//fichier IMC.js
//utilisation de fonction pour le calcule et l'interpretation de l'imc
//Laheyne Louis 23/04/2021

function patient(prmNom, prmPrenom, prmAge, prmSexe, prmTaille, prmPoids) {

    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.sexe = prmSexe;
    this.taille = prmTaille;
    this.poids = prmPoids;

    this.decrire = function () {
        let description = "";
        let tailleMetre = 0;
        let sexe = this.sexe;
        tailleMetre = this.taille / 100
        if (sexe == 'masculin') {
            description = "le patient " + this['prenom'] + " " + this['nom'] + " de sexe " + sexe + " est agé de " + this['age'] + "ans. Il mesure " + tailleMetre + "m et pèse " + this['poids'] + " kg";
        } else {
            description = "la patiente " + this['prenom'] + " " + this['nom'] + " de sexe " + sexe + " est agé de " + this['age'] + "ans. elle mesure " + tailleMetre + "m et pèse " + this['poids'] + " kg";
        }
        return description;
    }

    this.definir_corpulence = function () {
        let poids = 0;  //pour le poid dans la methode 
        let taille = 0; //pour la taille dans la methode 
        let imc = 0;    // pour le resultat de l'imc
        let def_corpulence = "";    //pour contenir la phrase de retour 
        let sexe = this.sexe;
        poids = this.poids; // recuperation du poid 
        taille = this.taille;  // recuperation de la taille 

        function calculer_IMC() {
            let valIMC;
            valIMC = poids / ((taille / 100) * (taille / 100)); //calcul de l'IMC
            return valIMC;
        };

        function interpreter_IMC() {
            let IMCsexe;
            let interpretation = "";

            if (sexe == 'masculin') {
                IMCsexe = imc - 2;
            } else {
                imcsexe = imc;
            }

            if (IMCsexe < 16.5) { //si l'imc est inferieur a 16.5 la personne est en denutrition
                interpretation = "Interpetation de l'IMC : Denutrition";
            }
            if ((IMCsexe >= 16.5) && (IMCsexe < 18.5)) { //si l'imc est superieur a 16.5 et inferieur a 18.5 la personne est en maigreur
                interpretation = "Interpetation de l'IMC : Maigreur";
            }
            if ((IMCsexe >= 18.5) && (IMCsexe < 25)) { //si l'imc est superieur a 18.5 et inferieur a 25 la personne a une corpulence normale
                interpretation = "Interpetation de l'IMC : corpulence normale";
            }
            if ((IMCsexe >= 25) && (IMCsexe < 30)) { //si l'imc est superieur a 25 et inferieur a 30 la personne est en surpoids
                interpretation = "Interpetation de l'IMC : surpoids";
            }
            if ((IMCsexe >= 30) && (IMCsexe < 35)) { //si l'imc est superieur a 30 et inferieur a 35 la personne est en obesite modere
                interpretation = "Interpetation de l'IMC : Obesite modere";
            }
            if ((IMCsexe >= 35) && (IMCsexe <= 40)) { //si l'imc est superieur a 35 et inferieur a 4 la personne est en obesite severe
                interpretation = "Interpetation de l'IMC : Obesite severe";
            }
            if (IMCsexe > 40) { //si l'imc est superieur a 40  la personne est en obesite morbide
                interpretation = "Interpetation de l'IMC : obesite morbide";
            }
            return interpretation;
        };
        imc = calculer_IMC();
        def_corpulence = "Son IMC est de : " + imc.toFixed(2) + "\n" + interpreter_IMC(imc)
        return def_corpulence;
    }


};



//Création des objets
let objPatient1 = new patient('Dupont', 'Jean', 30, 'masculin', 180, 85);
let objPatient2 = new patient('Moulin', 'Isabelle', 46, 'feminin', 158, 74);
let objPatient3 = new patient('Martin', 'Eric', 42, 'masculin', 165, 90);

//Affichage de la descritpion des objets
console.log(objPatient1.decrire());              //affichage de la description du patient
console.log(objPatient1.definir_corpulence());   //affichage de son imc et son interpretation
console.log(objPatient2.decrire());              //affichage de la description du patient
console.log(objPatient2.definir_corpulence());   //affichage de son imc et son interpretation
console.log(objPatient3.decrire());              //affichage de la description du patient
console.log(objPatient3.definir_corpulence());   //affichage de son imc et son interpretation


