//fichier IMC.js
//utilisation de fonction pour le calcule et l'interpretation de l'imc
//Laheyne Louis 23/04/2021

let objPatient = {
    //propriete
    nom: 'dupond',
    prenom: 'jean',
    age: 30,
    sexe: 'masculin',
    taille: 180,
    poids: 85,

    //methode
    decrire: function () {
        let description;
        let tailleMetre = 0;
        tailleMetre = this.taille / 100
        description = "le patient " + this['prenom'] + " " + this['nom'] + " de sexe " + this['sexe'] + " est agé de " + this['age'] + "ans. Il mesure " + tailleMetre + "m et pèse " + this['poids'] + " kg";
        return description;
    },

    definir_corpulence: function () {
        let poids = 0;  //pour le poid dans la methode 
        let taille = 0; //pour la taille dans la methode 
        let imc = 0;    // pour le resultat de l'imc
        let def_corpulence = "";    //pour contenir la phrase de retour 
        poids= this.poids ; // recuperation du poid 
        taille = this.taille ;  // recuperation de la taille 

        function calculer_IMC() {
            let valIMC;
            valIMC = poids / ((taille / 100) * (taille / 100)); //calcul de l'IMC
            return valIMC;
        };

        function interpreter_IMC(prmIMC) {
            let interpretation = "";

            if (prmIMC < 16.5) { //si l'imc est inferieur a 16.5 la personne est en denutrition
                interpretation = "Interpetation de l'IMC : Denutrition";
            }
            if ((prmIMC >= 16.5) && (prmIMC < 18.5)) { //si l'imc est superieur a 16.5 et inferieur a 18.5 la personne est en maigreur
                interpretation = "Interpetation de l'IMC : Maigreur";
            }
            if ((prmIMC >= 18.5) && (prmIMC < 25)) { //si l'imc est superieur a 18.5 et inferieur a 25 la personne a une corpulence normale
                interpretation = "Interpetation de l'IMC : corpulence normale";
            }
            if ((prmIMC >= 25) && (prmIMC < 30)) { //si l'imc est superieur a 25 et inferieur a 30 la personne est en surpoids
                interpretation = "Interpetation de l'IMC : surpoids";
            }
            if ((prmIMC >= 30) && (prmIMC < 35)) { //si l'imc est superieur a 30 et inferieur a 35 la personne est en obesite modere
                interpretation = "Interpetation de l'IMC : Obesite modere";
            }
            if ((prmIMC >= 35) && (prmIMC <= 40)) { //si l'imc est superieur a 35 et inferieur a 4 la personne est en obesite severe
                interpretation = "Interpetation de l'IMC : Obesite severe";
            }
            if (prmIMC > 40) { //si l'imc est superieur a 40  la personne est en obesite morbide
                interpretation = "Interpetation de l'IMC : obesite morbide";
            }
            return interpretation;
        };
        imc = calculer_IMC();
        def_corpulence = "Son IMC est de : " + imc.toFixed(2) +"\n"+ interpreter_IMC(imc)
        return def_corpulence;
    }
};

//affichage
console.log(objPatient.decrire())
console.log(objPatient.definir_corpulence())


